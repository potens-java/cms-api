package top.potens.cms;

import org.junit.jupiter.api.Test;
import top.potens.generator.MybatisGenerator;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className MybatisGeneratorTest
 * @projectName testpapioframework
 * @date 2020/1/16 15:54
 */
public class MybatisGeneratorTest {
    @Test
    void run() throws Exception {
        MybatisGenerator mybatisGenerator = MybatisGenerator.getInstance();
        mybatisGenerator.setJdbcConnectionURL("jdbc:mysql://127.0.0.1:3306/user?useSSL=false&serverTimezone=Hongkong")
                .setJdbcDriverClass("com.mysql.cj.jdbc.Driver")
                .setJdbcUserId("readonly")
                .setJdbcPassword("314106")
                .setModelTargetPackage("top.potens.cms.model")
                .setModelTargetProject("src/main/java")
                .setMapTargetPackage("auto")
                .setMapTargetProject("src/main/resources/mappings")
                .setClientTargetPackage("top.potens.cms.dao.db.auto")
                .setClientTargetProject("src/main/java")
                .addTable("t_area", "area_id", "Area")
                .addTable("t_channel", "channel_id", "Channel")
                .addTable("t_operator_log", "operator_log_id", "OperatorLog")
                .run();
    }
}
