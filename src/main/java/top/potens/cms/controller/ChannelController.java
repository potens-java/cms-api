package top.potens.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.potens.cms.response.ChannelDetailResponse;
import top.potens.cms.service.ChannelService;
import top.potens.core.model.ApiResult;

import javax.validation.constraints.NotNull;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ChannelController
 * @projectName cms-api
 * @date 2020/1/30 15:00
 */
@RestController
@RequestMapping("/channel")
@Api(description = "基本操作")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Validated
public class ChannelController {
    private final ChannelService channelService;
    @GetMapping("/by-code")
    public ApiResult<ChannelDetailResponse> getChannelByCode(
            @ApiParam(value = "channelCode", example = "self_tel") @RequestParam(required = true) @NotNull String channelCode
    ) {
        ApiResult<ChannelDetailResponse> result = new ApiResult<>();
        result.setData(channelService.getChannelByCode(channelCode));
        return result;
    }
}
