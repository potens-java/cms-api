package top.potens.cms.model;

import java.io.Serializable;
import java.util.Date;

public class OperatorLog implements Serializable {
    private Integer operatorLogId;

    private Integer relationId;

    private String relationType;

    private Date createTime;

    private Date updateTime;

    private Integer userId;

    private String userName;

    private static final long serialVersionUID = 1L;

    public Integer getOperatorLogId() {
        return operatorLogId;
    }

    public void setOperatorLogId(Integer operatorLogId) {
        this.operatorLogId = operatorLogId;
    }

    public Integer getRelationId() {
        return relationId;
    }

    public void setRelationId(Integer relationId) {
        this.relationId = relationId;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType == null ? null : relationType.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }
}