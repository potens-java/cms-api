package top.potens.cms.model;

import java.io.Serializable;

public class OperatorLogWithBLOBs extends OperatorLog implements Serializable {
    private String operatorDescAfter;

    private String operatorDescBefore;

    private static final long serialVersionUID = 1L;

    public String getOperatorDescAfter() {
        return operatorDescAfter;
    }

    public void setOperatorDescAfter(String operatorDescAfter) {
        this.operatorDescAfter = operatorDescAfter == null ? null : operatorDescAfter.trim();
    }

    public String getOperatorDescBefore() {
        return operatorDescBefore;
    }

    public void setOperatorDescBefore(String operatorDescBefore) {
        this.operatorDescBefore = operatorDescBefore == null ? null : operatorDescBefore.trim();
    }
}