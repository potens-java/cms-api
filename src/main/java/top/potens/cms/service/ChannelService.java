package top.potens.cms.service;

import top.potens.cms.response.ChannelDetailResponse;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ChannelService
 * @projectName cms-api
 * @date 2020/1/30 15:12
 */
public interface ChannelService {
    /**
    *
    * 方法功能描述:
    *
    * @author yanshaowen
    * @date 2020/1/30 15:22
    * @param channelCode
    * @return
    * @throws
    */
    ChannelDetailResponse getChannelByCode(String channelCode);
}
