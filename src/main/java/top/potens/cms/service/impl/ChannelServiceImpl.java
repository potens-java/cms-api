package top.potens.cms.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.potens.cms.curd.ChannelCurd;
import top.potens.cms.model.Channel;
import top.potens.cms.response.ChannelDetailResponse;
import top.potens.cms.service.ChannelService;
import top.potens.core.util.BeanCopierUtil;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ChannelServiceImpl
 * @projectName cms-api
 * @date 2020/1/30 15:12
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChannelServiceImpl implements ChannelService {
    private final ChannelCurd channelCurd;
    @Override
    public ChannelDetailResponse getChannelByCode(String channelCode) {
        Channel channel = channelCurd.selectByCode(channelCode);
        if (channel == null) {
            return null;
        }
        return BeanCopierUtil.convert(channel, ChannelDetailResponse.class);
    }
}
