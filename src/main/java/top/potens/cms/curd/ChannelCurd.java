package top.potens.cms.curd;

import top.potens.cms.model.Channel;
import top.potens.core.service.TableCommonService;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ChannelCurd
 * @projectName cms-api
 * @date 2020/1/30 15:13
 */
public interface ChannelCurd extends TableCommonService<Channel, Integer> {
    /**
    *
    * 方法功能描述: 根据code查询channel
    *
    * @author yanshaowen
    * @date 2020/1/30 15:16
    * @param channelCode
    * @return
    * @throws
    */
    Channel selectByCode(String channelCode);
}
