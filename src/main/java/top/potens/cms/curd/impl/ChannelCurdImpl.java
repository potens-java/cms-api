package top.potens.cms.curd.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.potens.cms.curd.ChannelCurd;
import top.potens.cms.dao.db.auto.ChannelMapper;
import top.potens.cms.model.Channel;
import top.potens.cms.model.ChannelExample;
import top.potens.core.service.impl.AbstractSimpleTableCommonServiceImpl;

import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ChannelCurdImpl
 * @projectName cms-api
 * @date 2020/1/30 15:14
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChannelCurdImpl extends AbstractSimpleTableCommonServiceImpl<Channel, Integer> implements ChannelCurd {
    private final ChannelMapper channelMapper;
    @Override
    protected Channel mapperByPrimaryKey(Integer id) {
        return channelMapper.selectByPrimaryKey(id);
    }

    @Override
    protected Channel mapperBySecondPrimaryKey(Integer id) {
        return null;
    }

    @Override
    protected Boolean isDelete(Channel channel) {
        return false;
    }
    @Override
    public Channel selectByCode(String channelCode) {
        ChannelExample channelExample = new ChannelExample();
        channelExample.createCriteria().andChannelCodeEqualTo(channelCode);
        List<Channel> channelList = channelMapper.selectByExample(channelExample);
        if (CollectionUtils.isEmpty(channelList)) {
            return null;
        }
        return channelList.get(0);
    }
}
