package top.potens.cms.dao.db.auto;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.potens.cms.model.OperatorLog;
import top.potens.cms.model.OperatorLogExample;
import top.potens.cms.model.OperatorLogWithBLOBs;

public interface OperatorLogMapper {
    long countByExample(OperatorLogExample example);

    int deleteByExample(OperatorLogExample example);

    int deleteByPrimaryKey(Integer operatorLogId);

    int insert(OperatorLogWithBLOBs record);

    int insertSelective(OperatorLogWithBLOBs record);

    List<OperatorLogWithBLOBs> selectByExampleWithBLOBs(OperatorLogExample example);

    List<OperatorLog> selectByExample(OperatorLogExample example);

    OperatorLogWithBLOBs selectByPrimaryKey(Integer operatorLogId);

    int updateByExampleSelective(@Param("record") OperatorLogWithBLOBs record, @Param("example") OperatorLogExample example);

    int updateByExampleWithBLOBs(@Param("record") OperatorLogWithBLOBs record, @Param("example") OperatorLogExample example);

    int updateByExample(@Param("record") OperatorLog record, @Param("example") OperatorLogExample example);

    int updateByPrimaryKeySelective(OperatorLogWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(OperatorLogWithBLOBs record);

    int updateByPrimaryKey(OperatorLog record);
}